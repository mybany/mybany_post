---
title: "IELTS Writing 3"
date: "2020-03-20T16:15:26+08:00"
categories:
    - English Learning
tags:
    - Writing
author: Ben
---

&ensp;&ensp;&ensp;&ensp;IELTS writing topics: Some people who have been successful in the society don't attribute their success to the theoretical knowledge they learned from their university. Do you agree that theoretical knowledge is not as valuable as expected?

<!--more-->

### 审题思路
&ensp;&ensp;&ensp;&ensp;此类同样是论述类题目，如果你同意，就论述同意的理由，如果你反对，就给出反对的理由，可以有折衷段。

### 自己思考的中文观点
1、理论知识是社会和科学发展的基础，国家的经济发展与建设离不开各个行业的理论知识
建设高楼大厦，修建桥梁，火箭发射以及财政和社会管理，都需要理论知识。。。。

2、高科技产业能够给社会带来巨大的财富，而这都是依据理论知识得到的
比如计算机行业，通讯行业以及集成电路行业，这些高科技行业都是基于数学、物理、电气等专业知识发展而来的，他们在服务人们的同时，也带来了巨大的财富

3、理论知识可以总结人们的经验，通过理论化的方法，能够摆脱人们积累经验的过程，节约人们非常多的时间，提高人们的效率
在工业发展中，理论知识能够使人们更加高效的运用机械，设计产品，而不是仅仅依据经验完成

参考观点里面全部都是缺点，因为题目说的很清楚使negative effect，干脆就全部写缺点，除非使想不出来缺点了，写一个优点应该也可以。

### 行文中英文及对比改正
1、关于理论知识对职业成功的贡献，人们一直都有不同的看法。

&ensp;&ensp;&ensp;&ensp;When talk about the contribution of theoretical knowledge to career success, people always have different views.

Reference:

&ensp;&ensp;&ensp;&ensp;People have different views about the contribution of theoretical knowledge on success

AAA对BBB贡献: contribution of AAA on BBB 注意介词是on,不是to

2、这是可以理解的，因为很多在事业上获得成功的人士并没有完成大学教育

&ensp;&ensp;&ensp;&ensp;This is acceptable as many successful people do not complete their collage education.

Reference:

&ensp;&ensp;&ensp;&ensp;This is understandable due to the fact that a considerable number of people who did not complete a university education have achieved success in their careers.

大量的人: a considerable number of people 

可以理解的: understandable

注意due to, 不要忘记这种表达

完成大学教育: complete a university education 注意中间的 a 

获得成功: achieve success 注意获得，不要忘了achieve

3、我认为大学学的东西对成功是至关重要的

&ensp;&ensp;&ensp;&ensp;In my view, knowledge learned in university is significant to sucess.

Reference:

&ensp;&ensp;&ensp;&ensp;In my view, what students have learnt at university is vitally important to a fulfilling career.

对成功至关重要: is vitally important to a fulfilling career

a fulfilling career: 成功的职业

直接用从句表达大学学的东西，不用想得太复杂了

4、通过学习理论，人们知道某个科目的概念和原则，因此可以成为某个行业的专业人士

&ensp;&ensp;&ensp;&ensp;By learning theoretical knowledge, people can understand the principles and concepts of a subject and be the professionals of an industry.

Reference:

&ensp;&ensp;&ensp;&ensp;By learning theories, people can gain a full understanding of concepts and principles of different subjects, so they can build expertise in their professions.

学习理论: learning theories

知道知识: gain a full understanding of XXX

成为某个行业的专业人士: build expertise in their professions.

5、例如，一个心理医生不可能确定有效的治疗，除非他(她)了解各种解释人们心理疾病的理论

&ensp;&ensp;&ensp;&ensp;For example, a psychologist are impossible to determine efficient treatment except he(she) knows many kinds of theories which can explain people's sychological disease.

Reference:

&ensp;&ensp;&ensp;&ensp;For example a psychiatrist cannot identity effective therapies, unless he or she is well-informed about all theories about different mental health problems.

心理学家: psychiatrist 

确定有效的治疗: identity effective therapies 确定identity   决定determine   治疗therapy  

除非: unless 这里接从句，不能用except for

对.....很了解: be well-informed about 

心理疾病的理论: all theories about different mental health problems

6、很多理论都是基于实践经验发展起来的，因此有现实的意义，而掌握这些理论的人可以有很好的工作前景，甚至因为革新而获得很多财富

&ensp;&ensp;&ensp;&ensp;Lots of theories are meaningful as they are based on practical experiences. People who understand those theoreis will have a excellent career prospects and even gain wealth because of innovation.

Reference:

&ensp;&ensp;&ensp;&ensp;Many theories have developed on the basis of empirical knowledge, so these theories have practical implications and poeple who are familiar with theories can have a good prospect and even earn a fortune with some innovations they have advanced.

发展: develop

基于XXX的基础: on the basis of XXX 

经验知识: empirical knowledge   (经验empirical)

实际影响: practical implications

有很好的前景: have a good prospect

获得财富: earn a fortune  (财富: fortune/wealth)

7、学习理论还有一个好处，就是提高人们解决问题的能力，还有创新能力

&ensp;&ensp;&ensp;&ensp;Another advantage of learning theories is that it can improve people's ability of solving problems and creativity.

Reference:

&ensp;&ensp;&ensp;&ensp;Another advantage of learning theories is that students can improve problem-solving abilities and exercise creative potential.

解决问题的能力: problem-solving abilities

锻炼创新能力: exercise creative potential

解决问题的能力和锻炼创新能力,不要下意识用there is

8、大学生可以提高研究能力，收集信息去了解不同的课题

&ensp;&ensp;&ensp;&ensp;College students are able to enhance research abilty and collect information to understand different subjects.

Reference:

&ensp;&ensp;&ensp;&ensp;University students can improve research skills and collect information on different subject matters.

提高研究能力: improve research skills

课题: subject matters


9、理论可以开拓人的思维，提高人的思辨能力，让人们可以使用不同理论去推动知识的进步

&ensp;&ensp;&ensp;&ensp;Theories can ______ , improve people's ___________ and allow people to use different theories to ______

Reference:

&ensp;&ensp;&ensp;&ensp;Theoretical knowledge could broaden people's minds and improve critical thinking skills, empowering them to push forward the boundaries of knowledge.

开拓人们的思维: broaden people's minds

思辨能力: critical thinking skills

让/允许: empower/allow

推动知识的进步: push forward the boundaries of knowledge

10、相比之下，没有上过大学的人可以参加一些实践训练去处理一些普遍问题，但是在面对一些非常规问题的时候就束手无策

&ensp;&ensp;&ensp;&ensp;In contrast, people who never go to university could take part in practical training to handle some common problems, but they will have no idea when they face some unusual problems.

Reference:

&ensp;&ensp;&ensp;&ensp;In contrast, some people who did not go to university can receive some practical training to deal with some general problems, but they may be helpless in the face of unusual problems.

面对非常规问题束手无策: be helpless in the face of unusual problems

实践训练: practical training

11、我们也需要承认一个事实: 理论知识不是人们成功的唯一因素

&ensp;&ensp;&ensp;&ensp;We need to recognise the fact that theoretical knowledge is not the only factor to success.

Reference:

&ensp;&ensp;&ensp;&ensp;Meanwhile, we must recognise the fact that theoretical knowledge is not the unique determinant of success.

连接词同时: meanwhile

承认一个事实: recognise the fact that

唯一因素: the unique determinant of 

12、有趣的是很多成功人士被发现有类似的性格特点

&ensp;&ensp;&ensp;&ensp;It is interesting to note that many successful people are found to have similar personalities.

Reference:

&ensp;&ensp;&ensp;&ensp;It is interesting to note that many successful people have some personality traits in common.

有趣的是: It is interesting to note that

有类似的性格特点: have some personality traits in common

trait: 特质

13、一个乐观、勤奋、积极、善于与人交往的人更加能够克服困难，和别人合作，从而能够获得成功

&ensp;&ensp;&ensp;&ensp;A man who is outlooking, hard-working, optimistic and good at getting along with others could be easier to overcome difficulties and achieve success by collobrate with parterners.

Reference:

&ensp;&ensp;&ensp;&ensp;An optimistic, hardworking and sociable person is able to overcome difficulties and cooperate with others to achieve success.

善于与人交往: sociable 

与他人合作: cooperate with others

14、另外一个因素可能是社会关系网，通过这个网络，人们可以获得信息、想法和资源

&ensp;&ensp;&ensp;&ensp;Another factor may be the social contact network. By that, people can get information, ideas and resources.

Reference:

&ensp;&ensp;&ensp;&ensp;Another factor might be social network, by which people can receive information, draw upon ideas and gain access to resources.

社会关系网: social network

获得信息、想法和资源: receive information, draw upon ideas and gain access to resources

获得想法: draw upon ideas

记住定语从句，这个地方非常典型!

15、例如，一些商业人士可以告诉他们怎么和政府保持良好的关系，而教科书上未必有这样的知识

&ensp;&ensp;&ensp;&ensp;For example, some businessmen can tell how to maintain a good relationship with governments while textbook cannot.

Reference:

&ensp;&ensp;&ensp;&ensp;For instance, some businesspeople can tell them how to maintain a good relationship with the government, and this kind of knowledge may not be available in textbooks.

this kind of knowledge may not be available in textbooks.

16、大学教育的价值没有被人们正确地认识，而人们可以更加有效地甚至有创意地解决问题

&ensp;&ensp;&ensp;&ensp;University education is not __________, and people can solve problems more effectively and even creately.

Reference:

&ensp;&ensp;&ensp;&ensp;Higher education has been undervalued, and people can handle many problems effectively and creatively with knowledge they have acquired at university.

没有被正确认识: has been undervalued

接受知识: acquire/gain/increase knowledge

17、我们同时需要承认，除了教育之外，人的性格和社会关系也是成功的因素

&ensp;&ensp;&ensp;&ensp;We must know that people's personalities and social relationship are also essential to success.

Reference:

&ensp;&ensp;&ensp;&ensp;On the other hand, we need to admit that personality and social relationships are also essential to success.

XXX的因素: be essential to XXX

### 范文参考

&ensp;&ensp;&ensp;&ensp;It is understandable that some young people today **drop out of college**, because there are successful entrepreneurs or business leaders who did not **complete a university degree**. In my view, what students have leart at university is vitally important to a **fulfilling career**.

&ensp;&ensp;&ensp;&ensp;By learning theories at university, people can **gain a full understanding** of concepts and principles of different subjects, so they can **build expertise** in their professions. Many theories have developed on the basis of **empirical knowledge**, so these theories have **practical implications** and people who are familiar with theories can deal with practical problems at work. For example, a psychiatrist cannot identify effective therapies, unless he or she attends some university courses to **gain an insight into** all theories about the causes of different **mental health problems**.

&ensp;&ensp;&ensp;&ensp;Academic study can also **improve the problem-solving abilities of learners** and **empower them to exercise creative potential**. University students need to collect information on different **subject matters** to finish essays and reports, so they gain experience in analysing different unfamilier matters and **tackling these problems effectively**. When they **embark on their careers**, they will show confidence in doing research when **problems surface**, and **come up with corresponding solutions**. In contrast, those people who did not go to university may **receive some practical training** to deal with some general tasks, but they may be helpless **in the face of** unusual problems

&ensp;&ensp;&ensp;&ensp;Meanwhile, we must recognise the fact that theoretical knowledge is not the only determinant of success. Many successful people have some **personality traits** in common. An optimistic, hardworking and sociable person is able to **overcome difficulties** and cooperate with others to achieve success. Another factor might be the **social network**, by which people can draw upon ideas of experts, investors and business managers, such as those managers who understand how to maintain a good relationship with clients, suppliers and government officials. This kind of knowledge many not be available in textbooks.

&ensp;&ensp;&ensp;&ensp;In a word, **higher education** has been undervalued, and people can handle many problems effectively and creatively with knowledge they have acquired at university. On the other hand, we need to admit that personality and **social relationships** are also essential to success.




## 版本控制

| 修改内容    | 时间        | 修改人   |
| :--------: | :--------:  | :-----: |
| Init       | 2020-03-20T16:15:26+08:00 | Ben |



