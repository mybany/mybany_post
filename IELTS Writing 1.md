---
title: "IELTS Writing 1"
date: "2020-03-20T16:15:26+08:00"
categories:
    - English Learning
tags:
    - Writing
author: Ben
---

&ensp;&ensp;&ensp;&ensp;IELTS writing topics: It is better for students to live away from home while studying at university than living with parents. To what extent do you agree or disagree?

<!--more-->

### 审题思路
&ensp;&ensp;&ensp;&ensp;此类题目是“agree or disagree”，是论述类题目，此类题目的范本是，开头1段，说这样做的好处1~2段、说这样做的坏处1~2段，总结1段，如果你觉得这个东西好处多与坏处，就好处2段，坏处1段，然后总结的时候偏向于好处即可。

### 行文中英文及对比改正
1、很多大学生现在要么住在学校的宿舍，要么和同学合租公寓

&ensp;&ensp;&ensp;&ensp;Nowadays most of college students choose to live in school dormitories or rent a flat with classmates.

Reference:

&ensp;&ensp;&ensp;&ensp;Many university students today have choosen university accommodation or shared a flat with their friends

住在大学宿舍: university accommodation

合租: shared a flat with friends

school dormitory用法应该是对的，rent a flat 虽然是对，但是with XXX显得很奇怪，没有找到例句这么用，参考译文的share用的精髓的多，share都可以用来表示合作干某事

2、我觉得这是一个成长的标志，虽然年轻人很难和父母每天保持交流

&ensp;&ensp;&ensp;&ensp;I think/believe it is a sign of growth though youth can hardly keep contact with their parents.

Reference:

&ensp;&ensp;&ensp;&ensp;I think it is a rite of passage though these young people may not be able to maintain day-to-day contact with their parents.

必经之路: a rite of passage

保持: keep/maintain/hold 但是要注意每一个的细微区别，这个只能去背，没有技巧

每天的: day-to-day 当作形容词用即可

a sign of growth简直是弱智翻译，尽量避免弱智翻译

3、离开父母居住意味着年轻人要学习如何独立生活

&ensp;&ensp;&ensp;&ensp;Living without parents means that the youth have to learn how to live an independent life.

Reference:

&ensp;&ensp;&ensp;&ensp;Living away from parents means that young people have to learn how to live an independnet life.

living away from parents 比 living without parents 要好一点

4、他们需要学会一些生活技能，如做家务、理财、和室友相处等

&ensp;&ensp;&ensp;&ensp;They need to learn some living skills such as doing housework, managing their money, making friends with roomates.

Reference:

&ensp;&ensp;&ensp;&ensp;They have to develop some life skills, such as doing housekeeping, managing finances and getting along with friends.

学会生活技能: develop some life skills

做家务: doing housekeeping

理财: managing finances

相处: get along with

 "相处"的表达老是忘记，一定要记住，是 get along with

5、有了这些技能，他们以后也可以很好的应对工作上的事情

&ensp;&ensp;&ensp;&ensp;After managing these skills, they could deal well with their works in the future.

Reference:

&ensp;&ensp;&ensp;&ensp;With these skills, they can handle many matters easily in the workplace.

应对工作上的事情: handle matters in the workplace

in the workplace 其实是比较好用的，应该记住

6、例如，他们可以懂得如何将一切都打理的井井有条，如果它们有整理房间的习惯

&ensp;&ensp;&ensp;&ensp;For example, they understand how to make everything neat in their life if they are used to clean room regularly.

Reference:

&ensp;&ensp;&ensp;&ensp;For example, they are more likely to keep everything organized if they have the habit of cleaning their bedroom

井井有条: keep organized

有习惯可以就直接翻译 have the habit of，用 be more likely to显得更委婉

7、此外，大学生也可以在社会生活中享受更多的自由，交一些新的朋友

&ensp;&ensp;&ensp;&ensp;What's more, college students can also enjoy more freedom in social life and make more friends.

Reference:

&ensp;&ensp;&ensp;&ensp;In addition, college students can enjoy more freedom in social life and make new friends.

此外 What's more / In addition

8、它们不用每天都回家，因此有更多的时间参加很多活动

&ensp;&ensp;&ensp;&ensp;Thay do not have to go home everyday so that thay could have more time taking part in many activities.

Reference:

&ensp;&ensp;&ensp;&ensp;They do not need to go home every day, so they have more time to do a wide range of activities.

很多活动: a wide range of activities

9、它们可以和同学一起去健身，参加讲座，去图书馆做作业，甚至一起打工

&ensp;&ensp;&ensp;&ensp;They may choose to go to gym, join a lecture, do homework at library and even do part-time job together with their friends.

Reference:

&ensp;&ensp;&ensp;&ensp;They can go to the gym with other students, attend lectures, do asignments in the library and even do part-time work together.

健身房: gym

10、不仅它们可以提高社会技能，以后也有更多的人脉可用

&ensp;&ensp;&ensp;&ensp;They will not only improve social skills, but also _______

Reference:

&ensp;&ensp;&ensp;&ensp;In addition to improving social skills, they also have more personal resources to draw upon in the future.

不仅: In addition to 注意后面接名词

提高社会技能: improve social skills

有更多的人脉可用: have more personal resources to draw upon

这一句话直接背下来，比较特殊

11、这个选择也有一定的缺陷，就是孩子和父母的接触机会太少，情感的联系变得脆弱

&ensp;&ensp;&ensp;&ensp;However, this choice has some disadvantages. That is students have little chance to contact with their parents and  _________

Reference:

&ensp;&ensp;&ensp;&ensp;The downside of this choice is that the contact with parents will decline and the emotional tie will suffer.

缺点: downside

联系: contact

情感联系: emotional tie

减弱: decline

注意情感联系和减弱和接触的表达

12、它们很少和父母一起吃饭，而它们的父母也不太能知道孩子的近况

&ensp;&ensp;&ensp;&ensp;They hardly eat with their parents and parents can hardly know their children's status.

Reference:

&ensp;&ensp;&ensp;&ensp;They hardly have a meal with their parents and cannot keep up to date with the news about their family.

吃饭: have a meal with

近况: keep up to date with the news about their family

吃饭用eat显得很奇怪，注意have a meal with的表达，近况记住，keep up 是跟上的意思

13、他们有时候会显得无助，如果有一些问题不能解决。

&ensp;&ensp;&ensp;&ensp;Sometimes they will be helpless if some problems cannot be solved.

Reference:

&ensp;&ensp;&ensp;&ensp;They may feel helpless, if they cannot solve some problems in their studies.

14、这可能对于他们以后对家庭生活的态度也有影响

&ensp;&ensp;&ensp;&ensp;This can have some influence to their attitude to family life.

Reference:

&ensp;&ensp;&ensp;&ensp;This may also affect their attitude towards family life.

影响别脑子里面只有influence, affect, effect这些都能够视情况而用

15、总结一下，年轻人离开父母居住是一个好的决定，虽然这会影响到他们和父母的关系

&ensp;&ensp;&ensp;&ensp;In conclusion, it's a good decision to live far away from parents for the young though it will affect their relation with parents.

Reference:

&ensp;&ensp;&ensp;&ensp;In conclusion, it is a good decision for university students to leave home, although it may have a amaging effect on their family relationship.

注意决定的名词形式 decision


### 范文参考

&ensp;&ensp;&ensp;&ensp;Many university students today have either chosen **university accommodation** or shared a flat with their friends. I think it is a **rite of passage** though these young people may not be able to **maintain day-to-day contact with** their parents.

&ensp;&ensp;&ensp;&ensp;Living away from means that young people have to learn how to **live an independent life**. They have to **develop some life skills**, such as doing housekeeping, **managing finances** and getting along with flatmates. With these skills, they can handle many matters easily once they **enter the workforce**. For example, they are more likely to keep everything organised if they have the habit of **cleaning their bedrooms**.

&ensp;&ensp;&ensp;&ensp;In addition, college students can enjoy more freedom in **social life** and make new friends. They do not need to go home every day, so they have more time to do a wide range of activities, including **going to the gym** with other students, doing group assignments and even do part-time work together. Moreover, partying, club activities and back packing allows these young people to enlarge the circle of friends, so they have personal resources to draw upon in future careers.

&ensp;&ensp;&ensp;&ensp;The downside of this choice is that the contact with parents will decline and the **emotional tie** will suffer. They hardly have a meal with their parents and cannot keep up with the news about their family. As they can tackle problems independently, they do not feel the need to visit their family and take advice from parents. This may also affect their attitude towards **family life**.

&ensp;&ensp;&ensp;&ensp;In conclusion, it is good decision for university students to **leave home**, although it may **have a damaging effect on** their **family relationship**.




## 版本控制

| 修改内容    | 时间        | 修改人   |
| :--------: | :--------:  | :-----: |
| Init       | 2020-03-20T16:15:26+08:00 | Ben |



