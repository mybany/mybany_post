---
title: "IELTS Writing 4"
date: "2020-03-20T16:15:26+08:00"
categories:
    - English Learning
tags:
    - Writing
author: Ben
---

&ensp;&ensp;&ensp;&ensp;Instead of asking the government to bear the cost of higher education, students should pay tuition fees themselves. Do you agree or disagree with this statement.

<!--more-->

### 审题思路
&ensp;&ensp;&ensp;&ensp;此类同样是论述类题目，如果你同意，就论述同意的理由，如果你反对，就给出反对的理由，可以有折衷段。

### 自己思考的中文观点
1、国家不承担高等教育的费用，可以把钱用于基础设施的建设和科研发展上，提高人民的生活水平

2、学生承担高等教育的费用，可以激励学生学习，让他们珍惜自己接受的教育机会

3、政府承担学费也有一定好处，可以让学生没有经济压力，更加专心的进行学习


### 行文中英文及对比改正
1、接受大学教育对很多人是重要的，因为他们找工作更加容易

&ensp;&ensp;&ensp;&ensp;Receiving higher education is important to most people as they can find job easilier.

Reference:

&ensp;&ensp;&ensp;&ensp;Receiving a university education is important to many young people, because they can find employment easily in the future.

找工作更容易: find employment easily

找工作的表达: seek credible employment/job search/find a job/get a job/look for a job/ 

2、那些支持政府增加大学教育投资的人认为有一些学生因为学费高昂而不能上大学

&ensp;&ensp;&ensp;&ensp;Those who support government to rise higher education investment believe that some students cannot enter college because of high tuition fee.

Reference:

&ensp;&ensp;&ensp;&ensp;Those people who support government spending on education think that some students do not enrol because of learning costs.

支持政府增加大学教育投资: support government spending on education

上学(注册登记): enrol

学习费用: learning costs

3、我的个人看法是学生应该承担学费，而条件不好的同学可以获得金钱上的帮助

&ensp;&ensp;&ensp;&ensp;In my opinion, students should afford tuition fee, while some students in poor condition could get financial help.

Reference:

&ensp;&ensp;&ensp;&ensp;My personal view is that students should pay tuition, while the disadvantaged can receive financial assistance from the government.

我的个人看法: my personal view

付学费: pay tuition

条件不好的人: the disadvantaged 

金钱上的帮助: financial assistance 

注意获得帮助receive assistance

4、学生如果交学费，他们会更加努力的学习，认真对待这个学习机会

&ensp;&ensp;&ensp;&ensp;If students afford tuition fee, they will be more hard-working and treat this education chance seriously.

Reference:

&ensp;&ensp;&ensp;&ensp;If higher education is not free of charge, students will study hard and take this educational opportunity seriously.

免费: free of charge

认真对待这个学习机会: take this education opportunity seriously

努力学习: study hard

5、他们知道读大学的成本很高，尽量完成所有的作业，通过考试，准时获得学位

&ensp;&ensp;&ensp;&ensp;They know the high cost of university, so they try their best to complete tasks, pass exams and get degree on time.

Reference:

&ensp;&ensp;&ensp;&ensp;They understand the financial cost of completing a degree, so they make a conscious effort to finish all assignments and pass all exams in order to gain the qualification on time.

获得学位不要用get, 先用 complete degree

make a conscious effort to:做出有意识的努力

任务除了task, 还有assignments

取得资格: gain qualification

6、如果政府承担费用，很多学生会无所谓，经常挂科

&ensp;&ensp;&ensp;&ensp;If governments pay the tuition fee, so many studetns will ________ and always fail to pass exams.

Reference:

&ensp;&ensp;&ensp;&ensp;In contrast, in cases where young people have free access to education, they will possibly take it for granted and fail exams from time to time.

免费上学:  have free access to educatioin

在XXX的情况下: in cases where

觉得无所谓/理所当然: take it for granted 

经常挂科: fail exams from time to time

7、交学费的另外一个好处就是减少政府的负担

&ensp;&ensp;&ensp;&ensp;Another advantage of paying for university is reducing governmnets payment.

Reference:

&ensp;&ensp;&ensp;&ensp;Another benefit of charging tuition fees is that it can lighten the burden on the government.

减轻政府的负担: lighten the burden on the government

好处: benefit 不要忘了benefit

收取学费: charge tuition fee

8、政府可以多花点钱去资助中学和小学教育，减少文盲率，让更多的年轻人有能力上大学

&ensp;&ensp;&ensp;&ensp;Governments can spend more money on middle and primary school to reduce ignorance people and enable more youngth to go to university.

Reference:

&ensp;&ensp;&ensp;&ensp;More money can be used in primary and secondary education, which can reduce illiteracy and prepare the next generation for university-level courses.

中学教育和小学教育: primary and secondary education


9、政府也可以给研究生课程的学生提供补助，促进科技的发展

&ensp;&ensp;&ensp;&ensp;What's more, governments can also provide financial aid to graduated students which promote technology development.

Reference:

&ensp;&ensp;&ensp;&ensp;Subsidies can be provided for those students who enrol in post-graduate courses to promote technological innovation.

补助金: subsidies

进入研究生课程: enrol in post-graduate courses

促进科技发展: promote technological innovation

10、另外一方面，低收入家庭的学生可以免除学费

&ensp;&ensp;&ensp;&ensp;On the other hand, _________ students can be free of tuition fee.

Reference:

&ensp;&ensp;&ensp;&ensp;On the other hand, young people from less well-off backgrounds can be exempted from tuition.

低收入家庭: less well-off backgrounds

免除学费: be exempted from tuition

11、这样可以鼓励这些年轻人读大学，提高知识和技能，最后找到好的工作

&ensp;&ensp;&ensp;&ensp;This will encourage youngth going to universities which can increase knowledge and develop skills, and finally help them find a good job.

Reference:

&ensp;&ensp;&ensp;&ensp;This can encourage these young people to attend college to acquire knowledge and skills, which can improve their career prospects.

上学: attend college

知识和技能: acquire knowledge and skills

找到好工作: improve their career prospects

12、这样可以减少贫富差距，建立一个公平的社会

&ensp;&ensp;&ensp;&ensp;This can reduce the gap between rich and poor to build a equality society.

Reference:

&ensp;&ensp;&ensp;&ensp;This can close the gap between haves and have-nots and help build a fair society.

建立一个公平的社会: build a fair society

13、如果他们需要交学费，他们可能会放弃学业，这样很难挖掘自己的潜能

&ensp;&ensp;&ensp;&ensp;If they need to pay tuition fee, they may give up their education opportunity and they will hardly discover their potential.

Reference:

&ensp;&ensp;&ensp;&ensp;Conversely, tuition fees may force them to drop out of college and make it difficult for them to reach their potential

思考句型，主动被动都可以适当考虑

迫使放弃学业: force XXX to drop out of college

挖掘潜能: reach their potential

14、综上所述，政府应该根据学生的需要提供帮助，以确保它们有接受教育的机会

&ensp;&ensp;&ensp;&ensp;In summary, governments should provide assistance according to students' needs to ensure they have an opportunity to receive an education.

Reference:

&ensp;&ensp;&ensp;&ensp;To summarise, the government should provide financial support according to students' needs to ensure that they have access to education.

能够上学: they have access to education

确保: ensure

15、贫困的学生应该免除学费，而废除学费不实际

Reference:

&ensp;&ensp;&ensp;&ensp;Students from deprived backgrounds should be exempt from tuition fee, while the abolition of nutrition for all students is not realisable.

贫困学生: students from deprived backgrounds

免除学费: be exempt from tuition fee


### 范文参考

&ensp;&ensp;&ensp;&ensp;**Receiving a university education** is important to many young people, because they can **find employment** easily in the future. Those people who support **government spending** on education think that some students do not **enrol** because of **learning costs**. My personal view is that students should pay tuition, while the disadvantaged can **receive financial assistance** from the government.

&ensp;&ensp;&ensp;&ensp;If higher education is not free of charge, students will study hard and take this **educational opportunity** seriously. They understand the **financial cost** of **completing a degree**, so they **make a conscious effort** to finish all assignments and pass all exams in order to **gain the qualification** on time. In contrast, in cases where they **have free access to education**, they will possibly **take it for granted** and **fail their exams** from time to time.

&ensp;&ensp;&ensp;&ensp;Another benefit of charging tuition fees is that it can **lighten the burden** on the government. More money can be used in **primary and secondary education**, which can **reduce illiteracy** and prepare the next generation for **university-level courses**. Additionally, subsidies can be provided for those students who enrol in post-graduate courses, and the investment in these talented young people can **promote technological innovation**.

&ensp;&ensp;&ensp;&ensp;On the other hand, **young people from less well-off backgrounds** can **be exempted from tuition**. They will have an incentive to **attend college** to acquire knowledge and skills, which can **improve their career prospects**. This can **close the gap between haves and have-nots** and help **build a fair society**. Conversely, **tuition fees** may force them to **drop out of college** and make it difficult for them to **reach their potential**.

&ensp;&ensp;&ensp;&ensp;To summarise, students **from deprived backgrounds** should be exempt from tuition fees, while the **abolition of tuition** for all students is not realistic.




## 版本控制

| 修改内容    | 时间        | 修改人   |
| :--------: | :--------:  | :-----: |
| Init       | 2020-03-20T16:15:26+08:00 | Ben |



