---
title: "IELTS Writing 2"
date: "2020-03-20T16:15:26+08:00"
categories:
    - English Learning
tags:
    - Writing
author: Ben
---

&ensp;&ensp;&ensp;&ensp;IELTS writing topics: In many countries traditional food is being replaced by international fast food. This has negative effects on both families and societies. To what extent do you agree or disagree?

<!--more-->

### 审题思路
&ensp;&ensp;&ensp;&ensp;此类题目是“agree or disagree”，是论述类题目，此类题目的范本是，开头1段，说这样做的好处1~2段、说这样做的坏处1~2段，总结1段，如果你觉得这个东西好处多与坏处，就好处2段，坏处1段，然后总结的时候偏向于好处即可。

### 自己思考的中文观点
1、快餐替代传统食物，对于社会的文化发展不利，传统美食是国家文化的代表。

2、快餐的营养价值较低，不利于家人健康，会使家人变得肥胖。

3、快餐也有一定的好处，比如使工作节奏变快，有助于更高效的办公

参考观点里面全部都是缺点，因为题目说的很清楚使negative effect，干脆就全部写缺点，除非使想不出来缺点了，写一个优点应该也可以。

### 行文中英文及对比改正
1、人们现在很喜欢光顾快餐店，因为快的生活节奏和工作时间的不稳定

&ensp;&ensp;&ensp;&ensp;People now love to have a meal in fast food restaurant as fast pace of life and unstable working time.

Reference:

&ensp;&ensp;&ensp;&ensp;Many people today frequent fast-food restaurants, because of the fast pace of life and irregular working hours.

快餐店: fast-food restaurants

生活节奏: the pace of life

不稳定的工作时间: irregular working hours

pace 和 restaurant都是可数名词，要记住用冠词

2、快餐在很多文化里已经威胁传统的烹饪方式，人们需要重视这一变化可能产生的影响

&ensp;&ensp;&ensp;&ensp;Fast-food has threaten traditional food in many cultures. People need to pay attention to the effect of this change. 

Reference:

&ensp;&ensp;&ensp;&ensp;Fast food has posed a threat to traditional cuisine in many cultures, and people should put emphasis on the problems that many arise from this change.

构成威胁: pose a threat to

传统烹饪: traditional cuisine

每天的: day-to-day 当作形容词用即可

重视: put emphasis on 

注意出现的翻译 arise,这个很常用

3、快餐在某些国家，如美国，已经成为一个健康问题，在那国家，肥胖人口迅速增加。

&ensp;&ensp;&ensp;&ensp;Fast-food has been a health problem in some country such as America. In USA, the population of fat people increases quickly.

Reference:

&ensp;&ensp;&ensp;&ensp;The fast food has become a health issue in countries like America, where the number of overweight people has increased dramatically.

肥胖: overweight

健康问题: health issue

注意肥胖的表达，还有人口迅速增加中的dramatically

4、这不仅因为快餐含高脂肪、糖分、盐分和卡路里，也因为快餐往往份量很大。

&ensp;&ensp;&ensp;&ensp;This is not noly because the fast food has much fat, sugger, salt and Calories, but also the ____

Reference:

&ensp;&ensp;&ensp;&ensp;It is not only because fast food is high in fat, suger, salt and Calories but also because fast food is served in large portions.

份量很大: is served in large portions

有很高的热量: be in high fat

卡路里: calories

注意份量很大以及有很高热量的表达，"be high in + 名词"

5、一般来说，患有肥胖症的人比普通体重的人更可能患有心脏病和其他健康疾病。

&ensp;&ensp;&ensp;&ensp;Generally, overweight people are easilier to get ____ and other healthy-sick than normal people.

Reference:

&ensp;&ensp;&ensp;&ensp;In general, people who suffer from obesity are more likely to counter heart disease than normal-weight people

一般来说: In general

心脏病: heart disease

患有病情: suffer from 

患有心脏病: counter heart disease

注意患病的两种表达，一种是suffer from 一种是counter from，普通体重normal-weight

6、这就意味着政府需要花更多的钱在医疗系统上，这可能会影响经济发展

&ensp;&ensp;&ensp;&ensp;This means that governments need spend more money on medical system and it may affect enconomy development.

Reference:

&ensp;&ensp;&ensp;&ensp;As a consequence, this means that governments have to pour more money in the medical system which may have an adverse effect on the economic development.

这就意味着(上下承接): As a consequence

花更多钱: pour more money in 

对XXX有坏的影响(影响XXX): have an adverse effect on XXX

经济发展: economic development

注意经济发展，不要再用economy了!

7、另外一个问题是它也会影响人们的生活方式，包括饮食习惯

&ensp;&ensp;&ensp;&ensp;Another problem is that it can affect people's life style including diet habits.

Reference:

&ensp;&ensp;&ensp;&ensp;Another problem is that it can affect people's way of life, including dietary habits.

生活方式: lifestyle / the way of life

饮食习惯: dietary habits

注意饮食习惯的表达，不要用名词形式，用形容词形式 dietary habits

8、传统食物的特点是食料，烹饪方式和口味的多样性，体现了烹饪文化在不同地区的演变

&ensp;&ensp;&ensp;&ensp;The specialities of traditional food  are the ___, the way of cooking and the diversity of taste which can show the evolution of cooking culture in different regions.

Reference:

&ensp;&ensp;&ensp;&ensp;Traditional means of food preparation are known for the diversity of ingredients, cooking styles and flavours, and many traditional dishes show the evolution of cuisine in different regions.

传统烹饪方式: traditional means of food preparation 

手段: means

原料/食料: ingredient

口味: flavours

传统菜: traditional foods/dishes

烹饪(美食): cuisine

9、目前，绝大部分的快餐连锁都是提供美国或者欧洲的食品，包括汉堡包、炸鸡和披萨饼

&ensp;&ensp;&ensp;&ensp;Nowadays most of fast food chain restaurants provide food from America or Europe, including hamburgers, fry chicken and pisa.

Reference:

&ensp;&ensp;&ensp;&ensp;Recently, most of fast food chains probide American and European food, including hamburger, fired chicken, and pizza.

目前: recently

连锁店: fast food chains (不需要加restaurants)

美国和欧洲食品: American and European food(此处不用of)

披萨: pizza(拼写错误)


10、人们的饮食逐渐变得单调，而一些传统菜谱可能消失

&ensp;&ensp;&ensp;&ensp;People's diet will _________  , and some traditional ______ may disapeer.

Reference:

&ensp;&ensp;&ensp;&ensp;People's diet is increasingly monotonous, while some traditional recipes may vanish.

消失: disappear (拼写错误)   vanish也可以用

逐渐单调: increasingly monotonous

菜谱: recipes

逐渐: increasingly / gradually

逐渐变得单调 be increasingly monotonous

11、快餐对家庭关系也有负面的影响， 因为人们不像以前那么频繁的参加家庭聚餐

&ensp;&ensp;&ensp;&ensp;Fast food have a bad influence on family relationship because people won't have a family meal frequently.

Reference:

&ensp;&ensp;&ensp;&ensp;Fast food also has an adverse effect on your family relationship, as people do not participate family meals as frequently as before.

不想以前那么频繁: as frequently as before 这个表达不能忘!

负面影响: an adverse effect on 

家庭聚餐: participate family meals

12、家庭聚餐一般来说提供家人一个相互交流，相互理解的机会

&ensp;&ensp;&ensp;&ensp;Generally, family meals can provide family members with an opportunity which communicate and understand each other.

Reference:

&ensp;&ensp;&ensp;&ensp;As a general rule, family meals provide family members with an opportunity to communicate with each other and improve mutual understanding.

相互理解: improve mutual understanding 

注意provide中间的with

13、人们经常在外面吃饭，可能就不大知道家里人的近况，很少能够顾及家人的感情需要

&ensp;&ensp;&ensp;&ensp;When people eat outside frequently, they may not know condition about family menbers and hardly consider about emotional needs of family members.

Reference:

&ensp;&ensp;&ensp;&ensp;People who always eat outside may not be able to keep up to date with family issues and respond to emotional needs of other family members.

感觉这句话翻译的感觉有点不对劲，注意家里人境况的翻译: keep up to date with family issues 以及顾及感情需要 respond to emotional needs of family members.

14、家庭越来越分散，而很多人在生活遇到问题的时候感到孤独和无助

&ensp;&ensp;&ensp;&ensp;Lots of people will feel lonely and helpless when have living with problem as family become _____________

Reference:

&ensp;&ensp;&ensp;&ensp;Families have become more and more dispersed, and many of us feel lonely and helpless when they have problems in daily lives.

生活中遇到问题是have problems in daily lives

注意more and more是可以用的，不用怕

15、因此，我赞同快餐的普及是一个很大的问题

&ensp;&ensp;&ensp;&ensp;Therefore, I agree with that the popularity of fast food is a big problem.

Reference:

&ensp;&ensp;&ensp;&ensp;I thus agree that the popularity of fast food is a menace.

同意后面可以直接跟从句,不需要加with

menace: 威胁

16、快餐不仅威胁人们的健康，也会破坏饮食文化和社会关系

&ensp;&ensp;&ensp;&ensp;Fast food not only threaten people's health, but also distroy the social relationship.

Reference:

&ensp;&ensp;&ensp;&ensp;They not noly pose a threat to people's health but also to our cuisine culture and social relationships.

威胁作为动词使用，有固定的搭配 pose a threat to XXX 记住这个表达

饮食的名词一定要记住: cuisine 

社会关系记住复数: social relationships

### 范文参考

&ensp;&ensp;&ensp;&ensp;Many people today **frequent fast-food restaurants**, because of the **fast pace of life** and **irregular working hours**. Fast food has **posed a threat to traditional cuisine** in many cultures, and people should not **overlook** the problems that may arise from this change.

&ensp;&ensp;&ensp;&ensp;The **strong appetite** for fast food has become a health issue in countries like America, where the umber of **overweight people** has incresed dramatically. This kind of food is not only **high in fat, sugar, salt and calories** but also **served in large portions**. This is why it is known as junk food and linked to **obesity epidemic**. Those who suffer from obesity are more likely to **contract heart diseases** than normal-weight people. This means that governments have to **pour more money** in the **medical system**, which may **have an adverse effect on** the economic development.

&ensp;&ensp;&ensp;&ensp;Another problem is that it can **threaten the diversity of the world's cuisine**. Traditional means of food preparation are known for the diversity of ingredients, cooking styles and flavours, and many traditional dishes show the evolution of cuisine in different regions. If fast food chains which provide American or European food, including hamburgers, fried chicken and pizzas, dominate the market, people will find that their diet will become increasingly monotonous and that some traditional recipes may vanish.

&ensp;&ensp;&ensp;&ensp;The downside of this choice is that the contact with parents will decline and the **emotional tie** will suffer. They hardly have a meal with their parents and cannot keep up with the news about their family. As they can tackle problems independently, they do not feel the need to visit their family and take advice from parents. This may also affect their attitude towards **family life**.

&ensp;&ensp;&ensp;&ensp;In conclusion, it is good decision for university students to **leave home**, although it may **have a damaging effect on** their **family relationship**.




## 版本控制

| 修改内容    | 时间        | 修改人   |
| :--------: | :--------:  | :-----: |
| Init       | 2020-03-20T16:15:26+08:00 | Ben |



